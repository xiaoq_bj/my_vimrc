## How to install Plug for vim

<pre>
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

git clone git@github.com:VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

yum install -y ctags
yum install -y the_silver_searcher

apt-get install -y universal-ctags
apt-get install -y silversearcher-ag

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
</pre>

### 手动下载插件
```
cd /root/.vim/plugged/
git clone git@github.com:mileszs/ack.vim.git
git clone git@github.com:ludovicchabant/vim-gutentags.git
git close git@github.com:scrooloose/nerdtree.git
git clone git@github.com:vim-scripts/taglist.vim.git
git clone git@github.com:vim-scripts/a.vim.git
git clone git@github.com:tomasiser/vim-code-dark.git
git clone git@github.com:tomasr/molokai.git
git clone git@github.com:majutsushi/tagbar.git
git clone git@github.com:adelarsq/vim-matchit.git
```


