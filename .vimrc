" plug part

call plug#begin('~/.vim/plugged')
Plug 'ludovicchabant/vim-gutentags'
Plug 'scrooloose/nerdtree'
Plug 'vim-scripts/taglist.vim'
Plug 'vim-scripts/a.vim'
Plug 'tomasiser/vim-code-dark'
Plug 'tomasr/molokai'
Plug 'majutsushi/tagbar'
Plug 'mileszs/ack.vim'
Plug 'https://github.com/adelarsq/vim-matchit'
call plug#end()

" set
syntax on
set wrap
set number
"set relativenumber
set showcmd
set wildmenu
set hlsearch
set scrolloff=5
setlocal list
set listchars=tab:>~,trail:.
" set mouse=a
set encoding=utf-8  " The encoding displayed.
set fileencoding=utf-8  " The encoding written to file.

filetype indent on
set cindent 
" set noexpandtab
set shiftwidth=4

set expandtab
set autoindent

" auto () {} []
inoremap ( ()<LEFT>
inoremap { {}<LEFT>
inoremap [ []<LEFT>

" cursor
set cursorline
set cursorcolumn

" 设置状态行显示常用信息
" %F 完整文件路径名
" %m 当前缓冲被修改标记
" %m 当前缓冲只读标记
" %h 帮助缓冲标记
" %w 预览缓冲标记
" %Y 文件类型
" %b ASCII值
" %B 十六进制值
" %l 行数
" %v 列数
" %p 当前行数占总行数的的百分比
" %L 总行数
" %{...} 评估表达式的值，并用值代替
" %{"[fenc=".(&fenc==""?&enc:&fenc).((exists("+bomb") && &bomb)?"+":"")."]"} 显示文件编码
" %{&ff} 显示文件类型
set statusline=%F%m%r%h%w%=\ [ft=%Y]\ %{\"[fenc=\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\"+\":\"\").\"]\"}\ [ff=%{&ff}]\ [asc=%03.3b]\ [hex=%02.2B]\ [pos=%04l,%04v][%p%%]\ [len=%L]

" 设置 laststatus = 0 ，不显式状态行
" 设置 laststatus = 1 ，仅当窗口多于一个时，显示状态行
" 设置 laststatus = 2 ，总是显式状态行
set laststatus=2

" a.vim
nnoremap <silent> <F12> :A<CR>
nnoremap <silent> <F10> :AV<CR>

" set taglist
let Tlist_WinWidth = 60
let Tlist_GainFocus_On_ToggleOpen = 1
let Tlist_Use_Right_Window = 1
nnoremap <silent> <F8> : TlistToggle<CR>

" set NERDTree
nmap <Leader>fl :NERDTreeToggle<CR>
let NERDTreeWinSize=32
let NERDTreeWinPos="left"
let NERDTreeShowHidden=1
let NERDTreeMinimalUI=1
let NERDTreeAutoDeleteBuffer=1
let NERDTreeIgnore = ['\.obj$', '\.pyc$', '\.o$', '\.d$']

map <F3> :NERDTreeMirror<CR>
map <F3> :NERDTreeToggle<CR>
map <C-l> :tabn<CR>
map <C-h> :tabp<CR>
map <C-n> :tabnew<CR>

" set gutentags
let g:gutentags_project_root = ['.root', '.svn', '.git', '.project', 'CMakeLists.txt']

let g:gutentags_ctags_tagfile = '.tags'

let s:vim_tags = expand('~/.cache/tags')
let g:gutentags_cache_dir = s:vim_tags

if !isdirectory(s:vim_tags)
   silent! call mkdir(s:vim_tags, 'p')
endif

let g:gutentags_ctags_extra_args = ['--fields=+niazS', '--extra=+q']
let g:gutentags_ctags_extra_args += ['--c++-kinds=+pxI']
let g:gutentags_ctags_extra_args += ['--c-kinds=+px']

" Ack search
let g:ackprg = 'ag -w --nogroup --nocolor --column --ignore "*.log" --ignore "*test*" --ignore-dir UT --ignore-dir build --ignore-dir objs'

"set t_Co=256
"set t_ut=
"colorscheme codedark
let g:molokai_original = 1
let g:rehash256 = 1
colorscheme molokai

" Set Tagbar
let g:tagbar_width = 20
"auto run tagbar when use vim
"autocmd VimEnter * nested :call tagbar#autoopen(1)
"tagbar window postion
let g:tagbar_right = 1
"let g:tagbar_left = 1

